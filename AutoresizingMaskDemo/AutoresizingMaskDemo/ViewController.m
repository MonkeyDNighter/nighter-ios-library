//
//  ViewController.m
//  AutoresizingMaskDemo
//
//  Created by 夜游 on 15/9/14.
//  Copyright (c) 2015年 夜游. All rights reserved.
//

#import "ViewController.h"

#define topSpace 64
#define kMargin 20

#define kTopViewHeight 44
#define kTopViewWidth 300

#define kTextLabelWidth 200
#define kTextLabelHeight 30

@interface ViewController ()

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *textLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setupUI];
    [self setupAutoresizingMask];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupUI {
    self.topView = [[UIView alloc] initWithFrame:CGRectMake(kMargin, topSpace, kTopViewWidth, kTopViewHeight)];
    
    //设置UILabel
    CGFloat textLabelLeft = (self.topView.frame.size.width - kTextLabelWidth) / 2;
    CGFloat textLabelTop = (self.topView.frame.size.height - kTextLabelHeight) / 2;
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(textLabelLeft, textLabelTop, kTextLabelWidth, kTextLabelHeight)];
    [self.textLabel setText:@"Nighter"];
    [self.textLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.topView setBackgroundColor: [UIColor redColor]];
    [self.textLabel setTextColor:[UIColor whiteColor]];
    [self.topView addSubview:self.textLabel];
    [self.view addSubview:self.topView];
}

-(void)setupAutoresizingMask {
    //UILabel的宽度按照父视图比例缩放
    [self.textLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    //topView到父视图的顶部、左边、右边的距离不变(注意注释和代码的顺序相对应)
    [self.topView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin ];
    
    CGFloat topViewWidth = [UIScreen mainScreen].bounds.size.width - kMargin * 2;
    [self.topView setFrame:CGRectMake(kMargin, topSpace, topViewWidth, kTopViewHeight)];
}

@end


























