//
//  ViewController.h
//  RKSwipeBetweenViewControllers
//
//  Created by 夜游 on 15/9/11.
//  Copyright (c) 2015年 夜游. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RKSwipeBetweenViewControllersDelegate <NSObject>

@end

@interface RKSwipeBetweenViewControllers: UINavigationController<UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray *viewControllersArray;
@property (nonatomic, weak) id<RKSwipeBetweenViewControllersDelegate> navDelegate;
@property (nonatomic, strong) UIView *selectionBar;
@property (nonatomic, strong) UIPageViewController *pageController;
@property (nonatomic, strong) UIView *navigationView;
@property (nonatomic, strong) NSArray *buttonText;

@end

