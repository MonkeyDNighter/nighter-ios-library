//
//  ViewController.m
//  RKSwipeBetweenViewControllers
//
//  Created by 夜游 on 15/9/11.
//  Copyright (c) 2015年 夜游. All rights reserved.
//

#import "RKSwipeBetweenViewControllers.h"

CGFloat X_BUFFER = 0.0;
CGFloat Y_BUFFER = 14.0;
CGFloat HEIGHT = 30.0;

CGFloat BOUNCE_BUFFER = 10.0;
CGFloat ANIMATION_SPEED = 0.2;
CGFloat SELECTOR_Y_BUFFER = 40.0;
CGFloat SELECTOR_HEIGHT = 4.0;

CGFloat X_OFFSET = 8.0;

@interface RKSwipeBetweenViewControllers ()

@property (nonatomic) UIScrollView *pageScrollView;
@property (nonatomic) NSInteger currentPageIndex;
@property (nonatomic) BOOL isPageScorllingFlag;
@property (nonatomic) BOOL hasAppearedFlag;//阻止reloading

@end

@implementation RKSwipeBetweenViewControllers

@synthesize viewControllersArray;
@synthesize selectionBar;
@synthesize pageController;
@synthesize navigationView;
@synthesize buttonText;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationBar.barTintColor = [UIColor colorWithRed:0.01 green:0.05 blue:0.06 alpha:1];
    self.navigationBar.translucent = NO;
    viewControllersArray = [[NSMutableArray alloc] init];
    self.currentPageIndex = 0;
    self.isPageScorllingFlag = NO;
    self.hasAppearedFlag = NO;
    
}

-(void)viewWillAppear:(BOOL)animated{
    if (!self.hasAppearedFlag) {
        [self setupPageViewController];
        [self setupSegmentButtons];
        self.hasAppearedFlag = YES;
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)setupPageViewController {
    pageController = (UIPageViewController *)self.topViewController;
    pageController.delegate = self;
    pageController.dataSource = self;
    [pageController setViewControllers:@[viewControllersArray[0]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    [self syncScrollView];
}

-(void)syncScrollView {
    for (UIView *view in pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            if (view == self.selectionBar) {
                NSLog(@"吴祁阳");
            }
//            NSLog(@"%@", view);
            self.pageScrollView = (UIScrollView *)view;
            self.pageScrollView.delegate = self;
        }
    }
}

-(void)setupSegmentButtons {

    navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationBar.frame.size.height)];
    NSInteger numControllers = [viewControllersArray count];
    
    if (!buttonText) {
        buttonText = [[NSArray alloc] initWithObjects:@"first", @"second", @"third", @"etc", nil];
    }
    
    for (int i = 0; i < numControllers; ++i) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(X_BUFFER+i*(self.view.frame.size.width - 2*X_BUFFER)/numControllers, Y_BUFFER, (self.view.frame.size.width - 2*X_BUFFER)/numControllers, HEIGHT)];
        [navigationView addSubview:button];
        button.tag = i;
        button.backgroundColor = [UIColor colorWithRed:0.03 green:0.07 blue:0.08 alpha:1];
        [button setTitle:buttonText[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(tapSegmentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    pageController.navigationController.navigationBar.topItem.titleView = navigationView;
//    [self.navigationBar addSubview:navigationView];
    
    [self setupSelector];
}

-(void) setupSelector {
    selectionBar = [[UIView alloc] initWithFrame:CGRectMake(X_BUFFER - X_OFFSET, SELECTOR_Y_BUFFER, (self.view.frame.size.width - 2*X_BUFFER) / [viewControllersArray count], SELECTOR_HEIGHT)];
    NSLog(@"%f, %f", selectionBar.frame.size.width, selectionBar.frame.size.height);
    selectionBar.backgroundColor = [UIColor greenColor];
    selectionBar.alpha = 0.8;
    [navigationView addSubview:selectionBar];
}

-(void)tapSegmentButtonAction: (UIButton *)button{
    if (!self.isPageScorllingFlag) {
        NSInteger tmpIndex = self.currentPageIndex;
        
        __weak typeof(self) weakSelf = self;
        //当跳转多个页面时(如当前在第1个页面，跳转到第4个页面，也需要播放2、3的跳转动画)
        //check left -> right
        if (button.tag > tmpIndex) {
            for (int i = (int)tmpIndex + 1; i <= button.tag; ++i) {
                [pageController setViewControllers:@[[viewControllersArray objectAtIndex:i]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL complete){
                    if(complete) {
                        [weakSelf updateCurrentPageIndex:i];
                    }
                }];
            }
        }
        
        //check right - left
        if (button.tag < tmpIndex) {
            for (int i = (int)tmpIndex - 1; i >= button.tag; --i) {
                [pageController setViewControllers:@[viewControllersArray[i]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL complete) {
                    if (complete) {
                        [weakSelf updateCurrentPageIndex:i];
                    }
                }];
            }
        }
    }
}

-(void)updateCurrentPageIndex:(int)newIndex {
    self.currentPageIndex = newIndex;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat xFromCenter = self.view.frame.size.width - scrollView.contentOffset.x;
    
    NSInteger xCoor = X_BUFFER + selectionBar.frame.size.width * self.currentPageIndex - X_OFFSET;
    selectionBar.frame = CGRectMake(xCoor-xFromCenter/[viewControllersArray count], selectionBar.frame.origin.y, selectionBar.frame.size.width, selectionBar.frame.size.height);
}

#pragma mark - Page View Controller Data Source
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSInteger index = [viewControllersArray indexOfObject:viewController];
    if(index == NSNotFound || index == 0) {
        return nil;
    }
    index--;
    return viewControllersArray[index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSInteger index = [viewControllersArray indexOfObject:viewController];
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == [viewControllersArray count]) {
        return nil;
    }
    
    return viewControllersArray[index];
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    if (completed) {
        self.currentPageIndex = [viewControllersArray indexOfObject:[pageController.viewControllers lastObject]];
    }
}

@end






















