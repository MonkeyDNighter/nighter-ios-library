//
//  AppDelegate.h
//  RKSwipeBetweenViewControllers
//
//  Created by 夜游 on 15/9/11.
//  Copyright (c) 2015年 夜游. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

