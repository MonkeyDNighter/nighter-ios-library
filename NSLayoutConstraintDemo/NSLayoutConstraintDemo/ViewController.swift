//
//  ViewController.swift
//  NSLayoutConstraintDemo
//
//  Created by 夜游 on 15/9/14.
//  Copyright (c) 2015年 夜游. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let margin: CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupUI() {
        let view = UIView()
        
        view.backgroundColor = UIColor.blueColor()
        self.view.addSubview(view)
        
        view.setTranslatesAutoresizingMaskIntoConstraints(false)
        //view底部到父视图的Y轴中心的约束:距离为-margin
        let bottomConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -margin)
        
        let topConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: margin)
        
        let leftConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: margin)
        
        let rightConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Right, multiplier: 1, constant: -margin)
        
        self.view.addConstraints([bottomConstraint, topConstraint, leftConstraint, rightConstraint])
    }

}

























