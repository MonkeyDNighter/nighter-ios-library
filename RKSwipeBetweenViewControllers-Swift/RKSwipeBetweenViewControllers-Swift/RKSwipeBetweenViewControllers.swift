//
//  ViewController.swift
//  RKSwipeBetweenViewControllers-Swift
//
//  Created by 夜游 on 15/9/14.
//  Copyright (c) 2015年 夜游. All rights reserved.
//

import UIKit

extension Int {
    var CGFloatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
}

class RKSwipeBetweenViewControllers: UINavigationController {
    
    var viewControllersArray:[UIViewController] = [UIViewController]()
    var selectionBar: UIView! //选择条
    var pageController: UIPageViewController!
    var navigationView: UIView!
    var currentPageIndex = 0
    var pageAnimated = false
    
    var buttonsText: [String] = ["first", "second", "third", "fourth", "five", "six", "seven"]
    var buttonColor: [UIColor] = [UIColor.redColor(), UIColor.grayColor(), UIColor.purpleColor(), UIColor.orangeColor(), UIColor.brownColor(), UIColor.cyanColor()]
    
    let FIX_BUTTON_POSITION_OFFSET: CGFloat = 8 //titleView和View的之间会有一小段间距，为了使得按钮填补间距，按钮位置要像左边移动
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationBar.barTintColor = UIColor(red: 0.01, green: 0.05, blue: 0.06, alpha: 1)
        self.navigationBar.translucent = false
        
        if let storyboard = storyboard {
            let vc1 = storyboard.instantiateViewControllerWithIdentifier("demo1") as! UIViewController
            let vc2 = storyboard.instantiateViewControllerWithIdentifier("demo2") as! UIViewController
            let vc3 = storyboard.instantiateViewControllerWithIdentifier("demo3") as! UIViewController
            let vc4 = storyboard.instantiateViewControllerWithIdentifier("demo2") as! UIViewController
            let vc5 = storyboard.instantiateViewControllerWithIdentifier("demo1") as! UIViewController
            let vc6 = storyboard.instantiateViewControllerWithIdentifier("demo3") as! UIViewController
            viewControllersArray = [vc1, vc2, vc3, vc4, vc5, vc6]
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupPageViewController()
        setupSegmentButtons()
    }
    
    func setupPageViewController() {
        pageController = self.topViewController as! UIPageViewController
        pageController.delegate = self
        pageController.dataSource = self
        pageController.setViewControllers([viewControllersArray[0]], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        syncScrollView()
    }
    
    func syncScrollView() {
        for v in pageController.view.subviews {
            if v.isKindOfClass(UIScrollView) {
                let scrollView = v as! UIScrollView
                scrollView.delegate = self
            }
        }
    }

    func setupSegmentButtons() {
        navigationView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.navigationBar.frame.size.height))
        self.navigationBar.addSubview(navigationView)
        
        
        let numControllers = viewControllersArray.count
        let buttonWidth = navigationView.bounds.size.width / CGFloat(numControllers)
        let buttonHeight = navigationView.bounds.size.height
        var xOffet:CGFloat = 0
    
        
        for i in 0 ..< numControllers {
            let button = UIButton(frame: CGRect(x: xOffet, y: 0, width: buttonWidth, height: buttonHeight))
            xOffet += buttonWidth
            navigationView.addSubview(button)
            button.tag = i
            button.backgroundColor = buttonColor[i]
            button.setTitle(buttonsText[i], forState: UIControlState.Normal)
            button.addTarget(self, action: "tapSegmentButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        }
        
        setupSelector()
    }
    
    func setupSelector() {
        selectionBar = UIView(frame: CGRect(x: 0, y: 40, width: navigationView.bounds.width / CGFloat(viewControllersArray.count), height: 4))
        selectionBar.backgroundColor = UIColor.greenColor()
        navigationView.addSubview(selectionBar)
    }

    func tapSegmentButtonAction(sender: UIButton) {
        var direction = self.currentPageIndex < sender.tag ? UIPageViewControllerNavigationDirection.Forward : UIPageViewControllerNavigationDirection.Reverse
        pageController.setViewControllers([viewControllersArray[sender.tag]], direction: direction, animated: pageAnimated, completion: {
                complete in
                    if complete {
                        self.currentPageIndex = sender.tag
                }
        })
        if !pageAnimated { //PageViewController不播放翻页动画，则selectionBar不能正常滚动，因此需要UIView自己实现动画
            let sysDuration = UIApplication.sharedApplication().statusBarOrientationAnimationDuration
            UIView.animateWithDuration(sysDuration, animations: {
                var posX = sender.tag.CGFloatValue * self.selectionBar.frame.width
                self.selectionBar.frame.origin.x = posX
            })
        }
    }
}

extension RKSwipeBetweenViewControllers: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var index = find(viewControllersArray, viewController)
        if var index = index {
            --index
            return index >= 0 ? viewControllersArray[index] : nil
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var index = find(viewControllersArray, viewController)
        if var index = index {
            ++index
            return index < viewControllersArray.count ? viewControllersArray[index] : nil
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [AnyObject], transitionCompleted completed: Bool) {
        if completed {
            let index = find(viewControllersArray, pageController.viewControllers.last as! UIViewController) //pageController.viewControllers数组有且仅有一个元素
            if let index = index {
                self.currentPageIndex = index
            }
        }
    }
}

extension RKSwipeBetweenViewControllers: UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let xFromCenter = self.view.frame.size.width - scrollView.contentOffset.x
        
        let selectionBarPosX = selectionBar.frame.width * CGFloat(self.currentPageIndex)
        selectionBar.frame = CGRect(x: selectionBarPosX - xFromCenter / CGFloat(viewControllersArray.count), y: selectionBar.frame.origin.y, width: selectionBar.bounds.width, height: selectionBar.bounds.height)
        //此处有一个问题，一旦PageViewController切换动画，切换到下一个ViewController时，contentOffset.x会重置为Screen.bounds.width,导致selectionBar会跳回到起点
        //解决方案是一旦PageViewController的切换动画完成，立即更新索引
    }
}

























